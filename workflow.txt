1. run counts.ipynb

get feature and target frequencies in data/train.counts
create number of levels truncated at frequency in data/train.truncated_nlevels.csv
    copy data/train.truncated_nlevels.csv to sheet nlevel baseline.xlsx
get feature freq conditioned on click in data/train.counts.by_click
get feature frequencies for test in data/test.counts

2. baseline.ipynb

get a gross estimation of logloss using single feature bayes
    copy result to baseline sheet in nlevel baseline.xlsx
generate figure/freq_and_cond_prob.png show prob of click within each level of feature along with the cum jfreq of each level

3. evaluation of simple freq counting on single feature.ipynb

evaluate baseline in train/test split with confidence

4. create_train_offsets.cpp to_vw.py

compile and run create_train_offsets.cpp; then run to_vw.py

shuffle the data and convert to vw format

5. vw

train
	vw --random_seed=0 --loss_function=logistic --link=logistic -d data/train.vw -c --cache_file data/train.vw.cache -f data/vw.model
test
	vw -t -i data/vw.model -d data/test.vw -p data/vw.pred
