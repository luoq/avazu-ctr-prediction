import pandas as pd

ftrain = open('data/train')
train_counts = pd.read_pickle('data/train.counts')

heads = ftrain.readline().strip().split(',')
feature_names = heads[2:]

def counter_to_order(x):
    return dict([(str(b),a) for a,b in enumerate(a for a, _ in x.most_common())])
label_encoders = [counter_to_order(train_counts[k]) for k in feature_names]

ftrain_x = open('data/train.x', 'w')
ftrain_y = open('data/train.y', 'w')
ftrain_id = open('data/train.id', 'w')

for line in ftrain:
    items = line.strip().split(',')
    ftrain_id.write(items[0]+'\n')
    ftrain_y.write(items[1]+'\n')
    labels = []
    for x, d in zip(items[2:], labels):
        labels.append(d[x])
    ftrain_x.write(' '.join(map(str,labels))+'\n')


ftest = open('data/test')
ftest_x = open('data/test.x', 'w')
ftest_id = open('data/test.id', 'w')

ftest.readline()
for line in ftest:
    items = line.strip().split(',')
    ftest_id.write(items[0]+'\n')
    labels = []
    for x, d in zip(items[1:], labels):
        labels.append(d[x])
    ftest_x.write(' '.join(map(str,labels))+'\n')
