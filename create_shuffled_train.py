import array
import random
offsets = array.array('Q')
offsets.frombytes(open('./data/train.offsets', 'rb').read())
offsets = list(offsets[1:])

random.seed(1433)
random.shuffle(offsets)

fin = open('./data/train')
fout = open('./data/train.shuffled', 'w')
fout.write(fin.readline())
for i in offsets:
    fin.seek(i)
    fout.write(fin.readline())
