import random
import array
n_samples = 1000000
offsets = array.array('Q')
offsets.frombytes(open('./data/train.offsets', 'rb').read())
offsets = list(offsets[1:])

random.seed(1433)
indices = random.sample(offsets, n_samples)

fin = open('./data/train')
fout = open('./data/train.samples', 'w')
fout.write(fin.readline())
for i in indices:
    fin.seek(i)
    fout.write(fin.readline())
