#!/usr/bin/env python
import sys
import gzip
if __name__ == '__main__':
    [fin_name, fout_name] = sys.argv[1:]
    fout = gzip.open(fout_name, 'w')
    fout.write(b'id,click\n')
    for ID, prob in zip(open('data/test.id','r'), open(fin_name, 'r')):
        fout.write('{},{}\n'.format(ID.rstrip(), prob.rstrip()).encode())
    fout.close()
