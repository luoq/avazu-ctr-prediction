#!/usr/bin/env python
import pandas as pd
ftrain = open('data/train')
train_counts = pd.read_pickle('data/train.counts') 
heads = ftrain.readline().strip().split(',')
feature_names = heads[2:]

import string

def counter_to_order(x, threshold=2):
    return dict([(str(b),a) for a,b in enumerate(a for a, b in x.most_common() if b>=threshold)])

class Parser:
    def __init__(self, threshold):
        self.threshold = threshold
        self.label_encoders = [counter_to_order(train_counts[k], threshold) for k in feature_names]
        self.hour_index = feature_names.index('hour')

    def __call__(self, line, is_train=True):
        items = line.strip().split(',')
        ID = items[0]
        if is_train:
            target = items[1]
            features = items[2:]
        else:
            target = None
            features = items[1:]
        feature_codes = []
        for i, x, e, l in zip(range(len(self.label_encoders)), features, self.label_encoders, string.ascii_lowercase):
            # extract hour from date_hour string
            if i == self.hour_index:
                feature_codes.append((l, int(x[-2:])))
            elif x in e:
                feature_codes.append((l,e[x]))
        feature_str = ' '.join('|{} {}'.format(a,b) for a,b in feature_codes)
        if is_train:
            return ID, ('1' if target=='1' else '-1') + ' ' + feature_str
        else:
            return ID, feature_str

if __name__ == '__main__':
    import array
    import random
    import sys
    
    offsets = array.array('Q')
    offsets.frombytes(open('./data/train.offsets', 'rb').read())
    offsets = list(offsets)[1:]
    random.seed(1433)
    random.shuffle(offsets)

    cutoff = int(sys.argv[1])
    parse_line = Parser(cutoff)

    ftrain_vw = open('data/train.{}.vw'.format(cutoff), 'w')
    ftrain_id = open('data/train.{}.id'.format(cutoff), 'w')
    for i in offsets:
        ftrain.seek(i)
        line = ftrain.readline()
        ID, line_vw = parse_line(line)
        ftrain_id.write(ID+'\n')
        ftrain_vw.write(line_vw+'\n')
     
    ftest = open('data/test')
    ftest.readline()
    ftest_vw = open('data/test.{}.vw'.format(cutoff), 'w')
    ftest_id = open('data/test.{}.id'.format(cutoff), 'w')
    for line in ftest:
        ID, line_vw = parse_line(line, is_train=False)
        ftest_id.write(ID+'\n')
        ftest_vw.write(line_vw+'\n')
