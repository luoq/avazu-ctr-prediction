offsets = reinterpret(Uint64, readbytes(open("data/train.offsets")))[2:end];

srand(2324);
shuffle!(offsets);

fin = open("data/train")
fout = open("data/train.shuffled", "w")
write(fout, normalize_string(readline(fin), newline2lf=true))
for i=offsets
    seek(fin, i)
    write(fout, normalize_string(readline(fin), newline2lf=true))
end
close(fin)
close(fout)
