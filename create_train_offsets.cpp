#include <iostream>
#include <fstream>
#include <cstdint>
using namespace std;
int main(){
#ifdef TEST
  ifstream train("data/train.head");
  ofstream train_offsets("data/train.head.offsets", ios::binary);
#else
  ifstream train("data/train");
  ofstream train_offsets("data/train.offsets", ios::binary);
#endif
  char c;
  uint64_t offset;
  train.seekg(0, train.end);
  uint64_t end = train.tellg();
  train.seekg(0, train.beg);
  offset = train.tellg();
  train_offsets.write((const char*)(& offset), sizeof(offset));
  while(train.get(c)){
    if(c=='\n'){
      offset = train.tellg();
      if(offset < end)
	train_offsets.write((const char*)(& offset), sizeof(offset));
    }
  }
  return 0;
}
