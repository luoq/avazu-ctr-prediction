#include <fstream>
#include <cstdint>
#include <iostream>
#include <algorithm>
#include <random>
#include <string>
using namespace std;
int main(){
  ifstream f_offsets("data/train.offsets");
  f_offsets.seekg(0, f_offsets.end);
  uint64_t n = f_offsets.tellg()/sizeof(uint64_t);
  f_offsets.seekg(0, f_offsets.beg);
  uint64_t* offsets = new uint64_t[n];
  f_offsets.read((char*)offsets, n*sizeof(uint64_t));

  shuffle(offsets+1, offsets+n-1, default_random_engine(1832));
  
  ifstream fin("data/train");
  ofstream fout("data/train.shuffled");
  string tmp;
  for(uint64_t i=0; i<n; i++){
    auto offset = offsets[i];
    fin.seekg(offset);
    getline(fin, tmp);
    if(tmp.size()>0 && tmp[tmp.size()-1]=='\r')
      fout<<tmp.substr(0, tmp.size()-1)<<endl;
    else
      fout<<tmp<<endl;
  }

  return 0;
}
