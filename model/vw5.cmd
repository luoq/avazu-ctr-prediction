# interaction between features with < 100 levels
vw --random_seed=0 --loss_function=logistic --link=logistic -d data/train.100.vw -c --cache_file data/train.100.vw.cache -f model/vw5.model -P 1e5 \
-q jh -q jt -q jv -q ji -q jf -q jq -q jp -q jb -q jc -q jm -q jn -q js -q ht -q hv -q hi -q hf -q hq -q hp -q hb -q hc -q hm -q hn -q hs -q tv -q ti -q tf -q tq -q tp -q tb -q tc -q tm -q tn -q ts -q vi -q vf -q vq -q vp -q vb -q vc -q vm -q vn -q vs -q if -q iq -q ip -q ib -q ic -q im -q in -q is -q fq -q fp -q fb -q fc -q fm -q fn -q fs -q qp -q qb -q qc -q qm -q qn -q qs -q pb -q pc -q pm -q pn -q ps -q bc -q bm -q bn -q bs -q cm -q cn -q cs -q mn -q ms -q ns

vw -t -i model/vw5.model -d data/test.100.vw -p model/vw5.pred
./create_submission.py model/vw5.pred model/vw5.pred.csv.gz
