vw --random_seed=0 --loss_function=logistic --link=logistic -d data/train.1000.vw -c --cache_file data/train.1000.vw.cache -f model/vw3.model -P 1e5
vw -t -i model/vw3.model -d data/test.1000.vw -p model/vw3.pred
./create_submission.py model/vw3.pred model/vw3.pred.csv.gz
