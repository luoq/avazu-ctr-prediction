libFM -task c -train data/train.100.libsvm -test data/test.100.libsvm -dim '1,1,10' -seed 0 -verbosity 1 -out model/libfm1.pred -method sgd -learn_rate 0.01 -init_stdev 0.1 -iter 3

./create_submission.py model/libfm1.pred model/libfm1.pred.csv.gz
