vw --passes 10 --random_seed=0 --loss_function=logistic --link=logistic -d data/train.vw -c --cache_file data/train.vw.cache -f model/vw10.model
vw -t -i model/vw10.model -d data/test.vw -p model/vw10.pred
./create_submission.py model/vw10.pred model/vw10.pred.csv.gz
