vw --random_seed=0 --loss_function=logistic --link=logistic -d data/train.100.vw -c --cache_file data/train.100.vw.cache -f model/vw2.model
vw -t -i model/vw2.model -d data/test.100.vw -p model/vw2.pred
./create_submission.py model/vw2.pred model/vw2.pred.csv.gz
