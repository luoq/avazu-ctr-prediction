libFM -task c -train data/train.10000.libsvm -test data/test.10000.libsvm -dim '1,1,10' -seed 0 -verbosity 1 -out model/libfm3.pred -method sgd -learn_rate 0.01 -init_stdev 0.1 -iter 3

./create_submission.py model/libfm3.pred model/libfm3.pred.csv.gz
