vw --random_seed=0 --loss_function=logistic --link=logistic -d data/train.vw -c --cache_file data/train.vw.cache -f model/vw.model
vw -t -i model/vw.model -d data/test.vw -p model/vw.pred
./create_submission.py model/vw.pred model/vw.pred.csv.gz
