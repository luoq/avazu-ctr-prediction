import array
def get_offsets(filename):
    offsets = array.array('Q')
    with open(filename) as f:
        offsets.append(f.tell())
        while f.readline():
            offsets.append(f.tell())
    return offsets
offsets = get_offsets('data/train')
offsets.tofile(open('data/train.offsets.2', 'wb'))
